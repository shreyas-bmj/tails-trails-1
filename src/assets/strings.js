export const APP_STRINGS = {
  HOME: "Home",
  ABOUT_US: "About Us",
  OUR_SERVICE: "Our Service",
  CONTACT_US: "Contact Us",
  TERMS_SERVICES: "TERMS_SERVICES",

  PHONE_NO: "+91-94XXXXXXXX",
  ABOUT: "About",
  QUICK_LINKS: "Quick Links",
};
