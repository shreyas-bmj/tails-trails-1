import React from "react";

const LogoSVGComponent = (props) => (
  <svg width={275} height={50} viewBox="0 0 450 71" {...props}>
    <defs>
      <style>
        {
          ".a,.c,.d,.e{fill:#262626;}.a{stroke:#707070;}.b{clip-path:url(#a);}.d{font-size:40px;font-family:Roboto-Bold, Roboto;font-weight:700;letter-spacing:0.021em;}.e{font-size:20px;font-family:Roboto-Medium, Roboto;font-weight:500;}"
        }
      </style>
      <clipPath id="a">
        <rect
          className="a"
          width={74}
          height={62}
          transform="translate(469 272.508)"
        />
      </clipPath>
    </defs>
    <g transform="translate(-283 -369)">
      <g className="b" transform="translate(-186 101.492)">
        <g transform="translate(476.326 273)">
          <path
            className="c"
            d="M59.384,11.652a2.007,2.007,0,0,0-.21-1.8,1.861,1.861,0,0,0-1.546-.841H52.84A8.74,8.74,0,0,0,44.262.121a8.36,8.36,0,0,0-5.439,2.017A5.445,5.445,0,0,1,41.9,4.682c4.395,7.9,4.395,10.279,4.395,11.3a9.246,9.246,0,0,1-9.065,9.4A8.762,8.762,0,0,1,32.7,24.109l-18.41,8.562a16.188,16.188,0,0,0-9.2,14.583c-1.345-2.68-1.989-7.062.762-13.9a2.064,2.064,0,0,0-1.056-2.664,1.94,1.94,0,0,0-2.57,1.095C-1.333,40.616.033,46.315,1.8,49.541a12.944,12.944,0,0,0,3.653,4.214,10.431,10.431,0,0,0,9.972,7.873H29.834a2.717,2.717,0,0,0,0-5.431H24.42a10.577,10.577,0,0,0,1.385-5.622,8.938,8.938,0,0,0-3.44-7.312,2.092,2.092,0,0,1-.4-2.851A1.92,1.92,0,0,1,24.716,40a13.082,13.082,0,0,1,4.91,8.779L36.382,45.6V58.914A2.669,2.669,0,0,0,39,61.629h5.26a2.717,2.717,0,0,0,0-5.431H41.621V42.789a12.867,12.867,0,0,0,4.846-10.117V24.619a4.08,4.08,0,0,1,4-4.151h2.244A5,5,0,0,0,57.38,17.13ZM46.925,9.761a1.155,1.155,0,1,1,1.113-1.154A1.134,1.134,0,0,1,46.925,9.761Z"
            transform="translate(0 -0.121)"
          />
          <path
            className="c"
            d="M48.419,23.057a5.233,5.233,0,0,0,5.136-5.325c0-1.988-2.348-6.528-3.869-9.26a1.436,1.436,0,0,0-2.533,0c-1.521,2.732-3.87,7.273-3.87,9.261A5.233,5.233,0,0,0,48.419,23.057Z"
            transform="translate(-11.185 -1.876)"
          />
        </g>
      </g>
      <text className="d" transform="translate(378 406)">
        <tspan x={0} y={0}>
          {"TAILS AND TRAILS"}
        </tspan>
      </text>
      <text className="e" transform="translate(378 435)">
        <tspan x={0} y={0}>
          {"UNLEASHING LOVE"}
        </tspan>
      </text>
    </g>
  </svg>
);

export default LogoSVGComponent;
