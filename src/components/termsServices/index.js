import React, { useEffect, useRef } from "react";
import { Link } from "react-router-dom";
import "./styles.scss";

const data1 = [
  "The customer warrants and certifies that He/She is the owner or person responsible for the dog(s) brought to DogNation, 18 3rd Cross Rd., Rustam Bagh main Rd., Bangalore, Karnataka 560017 for a availing the services offered.",
  "They agree that they are ultimately responsible for determining whether boarding or daycare and pool activities are appropriate for the dog(s). They further agree that they are responsible for any risk posed by undisclosed medical conditions or behavior concerns.",
  "They acknowledge that should they give permission for the dog(s) to participate in group play activities, they recognize the possible risks involved in this activity, including but not limited to minor nicks and scratches and accept full responsibility for any illness or injury that might happen to the dog(s), and accept full financial responsibility for any charges that may occur.",
];

const data2 =
  "Unless explicitly requested not to do so, customer understands that their dog will engage in a customized exercise program while in the care of DogNation. They further understand the workout program created for their dog(s) and corresponding outcome are dependent upon a number of factors including but not limited to the condition and age of the dog.";
const data3 =
  "Customer understands they are responsible for, and agree to provide DogNation staff with the latest complete information about their dog(s), including underlying medical conditions, medications, physical limitations, behavior concerns, veterinarian doctor’s name and contact information, and veterinarian recommendations and limitations for the dog(s) brought to DogNation.";
const data4 =
  "Customer also accepts full responsibility for any damage or injury to persons, property or animals arising out of the dog’s participation in daycare, use of the grounds, facility, or pool, and the actions and conduct of the customers and the dog(s). Accordingly they agree to indemnify DogNation and its owners, employees, and independent contractors, for monetary damages and attorney fees; and further waive all personal claims and releases against DogNation, its owners, employees, and independent contractors for damage, injury or death sustained by the customers, arising out of their dog’s participation in the activities and services provided by DogNation or Anubis Lifestyle Pvt. Ltd., or presence on or use of the premises where services are performed; and further waive subrogation claims of insurers.";
const data5 =
  "Customers understand that by allowing the dog(s) to participate in any boarding/daycare, Event or Activity program offered through DogNation, they give permission for staff to take photographs, and/or videos, and to use the images or videos of the dog in printed matter, internet sites, or other promotional or advertising capacities. Photographs and videos are the property of DogNation and Anubis Lifestyle Pvt. Ltd.";
const data6 =
  "As a client of DogNation, customer understands that their dog(s) and any person they bring onto the property enter / swim / participate at their own risk.";

export default function TermsServices() {
  const tcRef = useRef(null);
  useEffect(() => {
    window.scrollTo({ top: 0, behavior: "smooth" });
  }, []);

  return (
    <div className="ts-container" ref={tcRef}>
      <div className="ts-header-container-background" />
      <div className="ts-header-container">
        <div className="ts-header-margin">
          <Link to={"/"}>
            <p className="ts-header-text1">HOME</p>
          </Link>
          <p className="ts-header-text2">Terms of Service</p>
        </div>
      </div>
      <div className="ts-image-container">
        <div className="ts-image-container-wrapper">
          <p className="ts-image-text">TERMS OF SERVICE</p>
        </div>
      </div>
      <div className="ts-content-container-wrapper">
        <div className="ts-content-container">
          <div className="ts-content">
            <p className="ts-content-title">
              By choosing to avail our services:
            </p>
            <ul className="ts-content-ul">
              {data1.map((item, index) => (
                <li
                  className="ts-content-text ts-content-li"
                  key={`TermsServices-${index.toString()}`}
                >
                  {" "}
                  {item}
                </li>
              ))}
            </ul>
          </div>
          <div className="ts-content ">
            <p className="ts-content-title ">ACTIVITIES</p>
            <p className="ts-content-text ts-content-li"> {data2}</p>
          </div>
          <div className="ts-content ">
            <p className="ts-content-title ">MEDICAL INFORMATION & VET</p>
            <p className="ts-content-text ts-content-li"> {data3}</p>
          </div>
          <div className="ts-content ">
            <p className="ts-content-title ">THIRD PARTY DAMAGES</p>
            <p className="ts-content-text ts-content-li"> {data4}</p>
          </div>
          <div className="ts-content ">
            <p className="ts-content-title ">VIDEOS & IMAGES</p>
            <p className="ts-content-text ts-content-li"> {data5}</p>
            <p className="ts-content-text ts-content-li"> {data6}</p>
          </div>
        </div>
      </div>
    </div>
  );
}
