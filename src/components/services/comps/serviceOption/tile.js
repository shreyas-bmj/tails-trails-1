import React, { forwardRef } from "react";
import "./styles.scss";

//so = service option;
const Tile = forwardRef(({ data, onTileClick }, ref) => {
  return (
    <div className="so-tile-container">
      <div
        className="so-tile-header-container"
        onClick={() => onTileClick(data.id)}
      >
        <img className="so-tile-header-image" src={data.icon} />
        <div className="so-tile-header-text-divider" />
        <div className="so-tile-header-text-wrapper">
          <p className="so-tile-header-text">{data.headerText}</p>
        </div>
      </div>
      <div className="so-tile-body">
        <p className="so-tile-body-title">{data.title}</p>
        <ul className="so-tile-body-ul">
          {data.content?.map((val, index) => (
            <li className="so-tile-body-li" key={`Tile-${index.toString()}`}>
              <span className="so-tile-body-li-icon">&#10003;</span>
              <p className="so-tile-body-li-text">{val}</p>
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
});

export default Tile;
