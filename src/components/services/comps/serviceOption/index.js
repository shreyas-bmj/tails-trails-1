import React, { useContext } from "react";
import { SafeAreaContext } from "../../../utiles/saveArea";
import {
  serviceOptionData1,
  serviceOptionData2,
  serviceOptionData3,
  serviceOptionData4,
} from "../../serviceData";
import Tile from "./tile";

export default function ServiceOption() {
  const { globalState, setGlobalState } = useContext(SafeAreaContext);
  const onClickHandle = (e) => {
    setGlobalState(e);
  };

  return (
    <div className="so-container">
      <div className="so-wrapper">
        <Tile data={serviceOptionData1} onTileClick={onClickHandle} />
        <Tile data={serviceOptionData2} onTileClick={onClickHandle} />
        <Tile data={serviceOptionData3} onTileClick={onClickHandle} />
        <Tile data={serviceOptionData4} onTileClick={onClickHandle} />
      </div>
    </div>
  );
}
