import React, { forwardRef } from "react";
import Dog from "../../../../assets/icons/dog.svg";
import Bone from "../../../utiles/bone";
import "./styles.scss";

const data = [
  {
    id: "ft-1",
    icon: Dog,
    text: "Superior Healthy Food (see protein content in packaged food",
  },
  {
    id: "ft-2",
    icon: Dog,
    text: "Superior Healthy Food (see protein content in packaged food",
  },
  {
    id: "ft-3",
    icon: Dog,
    text: "Superior Healthy Food (see protein content in packaged food",
  },
  {
    id: "ft-4",
    icon: Dog,
    text: "Superior Healthy Food (see protein content in packaged food",
  },
];

const FoodTile = forwardRef((props, ref) => {
  return (
    <div className="ft-container" ref={ref}>
      <div className="ft-wrapper">
        <div className="ft-left-container">
          <div className="ft-header-container">
            <p className="ft-header-text">
              Fresh Human-Grade Food
              <span className="ft-header-text-blue">
                {" "}
                For Your Buddy – PawPlate!
              </span>
            </p>
          </div>
          <div className="ft-content-main">
            {data.map((val, index) => (
              <div className="ft-content-container" key={val.id}>
                <img className="ft-content-image" src={val.icon} />
                <p className="ft-content-text">{val.text}</p>
              </div>
            ))}
          </div>
        </div>
        <div className="ft-right-container">
          <div className="ft-right-image-wrapper">
            <img
              className="ft-right-image"
              src={
                "http://dognation.pet/wp-content/uploads/2019/08/inner_images_01-1.png"
              }
            />
          </div>
          <div className="ft-right-bone-container">
            <Bone />
          </div>
        </div>
      </div>
    </div>
  );
});

export default FoodTile;
