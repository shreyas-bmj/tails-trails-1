import React, { forwardRef } from "react";
import Bone from "../../../utiles/bone";
import ServiceAccordion from "../accordion";
import "./styles.scss";

const ServiceContent = forwardRef(({ data }, ref) => {
  return (
    <div className="sc-container" ref={ref}>
      <div className="sc-left-container">
        <div className="sc-title-container">
          <p className="home-title-text sc-title">
            {data.title_1}
            <span className="home-title-text-yellow sc-title-yellow">
              {" "}
              {data.title_2}
            </span>
          </p>
        </div>
        <div className="sc-divider" />
        {/* <div className="sc-description-container">
          <p className="sc-description-text">
            A 6, 000 sqft ‘Poochy’ paradise with complete Dog services, a café,
            workspace and a pool… designed solely for the comfort of Dogs and
            their Hu-maan friends right in the heart of the city !
          </p>
        </div>
        <div className="sc-divider" /> */}
        <div className="sc-accordion-container">
          {data.accordion?.map((val, index) => (
            <ServiceAccordion
              data={val}
              key={`ServiceContent-${index.toString()}`}
            />
          ))}
        </div>
        <div className="sc-right-bone-container">
          <Bone />
        </div>
      </div>
      <div className="sc-right-container">
        <div className="sc-right-container-main">
          <div className="sc-right-container-sub-container">
            <img className="sc-right-image" src={data.image} />
          </div>
        </div>
      </div>
    </div>
  );
});

export default ServiceContent;
