import React from "react";
import "./styles.scss";

export default function RateTile({ data }) {
  return (
    <div className="rt-container">
      <div className="rt-wrapper">
        <div className="rt-divider" />
        <div className="rt-header-container">
          <img className="rt-header-icon" src={data.icon} />
          <p className="rt-header-text">{data.title}</p>
        </div>
        <div className="rt-content-container">
          {data.content.map((val, index) => (
            <React.Fragment key={`RateTile-${index.toString()}`}>
              <div className="rt-divider" />
              <div className="rt-content-wrapper">
                <span className="rt-content-title">{val.contentTitle}</span>
                <div className="rt-content-dots" />
                <span className="rt-content-rate">{val.amount}</span>
              </div>
            </React.Fragment>
          ))}
        </div>
        {data.extraNote ? (
          <p className="rt-content-extra-note">{data.extraNote}</p>
        ) : (
          <></>
        )}
      </div>
    </div>
  );
}
