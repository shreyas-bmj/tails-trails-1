import Accordion from "@material-ui/core/Accordion";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import CheckIcon from "@material-ui/icons/Check";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import React from "react";
import RateTile from "../rateTile";
import "./styles.scss";

export default function ServiceAccordion({ data }) {
  return (
    <div className="sa-accordion-container">
      <Accordion
      // expanded={expanded === data.accordionId}
      // onChange={handleChange(data.accordionId)}
      >
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id={`panel1a-header-${data.id}`}
        >
          <div className="sa-accordion-tick-icon-container flex-row ">
            <CheckIcon className="sa-accordion-tick-icon" />
          </div>
          <p className="sa-accordion-title">{data.title}</p>
        </AccordionSummary>
        <AccordionDetails>
          <>
            {data.type === "rateCard" ? (
              <div>
                {data.description.map((val, index) => (
                  <RateTile
                    data={val}
                    key={`ServiceAccordion-${index.toString()}`}
                  />
                ))}
              </div>
            ) : (
              <p className="sa-accordion-para">{data.description}</p>
            )}
          </>
        </AccordionDetails>
      </Accordion>
    </div>
  );
}
