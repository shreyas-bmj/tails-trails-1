import dog from "../../assets/icons/dog.svg";

const serviceContent1 = {
  title_1: "Dog Boarding &",
  title_2: "Day Care",
  image: "http://dognation.pet/wp-content/uploads/2019/07/hero_image_10-1.png",
  accordion: [
    {
      id: "1",
      accordionId: "wos-a-1",
      title: "Dog Grooming",
      description:
        "Grooming services to make your buddy feel like a king/queen.",
      type: "text",
    },

    {
      id: "2",
      accordionId: "wos-a-2",
      title: "Spa",
      description: "Let us pamper your dog the way they are supposed to be.",
      type: "text",
    },

    {
      id: "3",
      accordionId: "wos-a-3",
      title: "View Rate Card",
      type: "rateCard",
      description: [
        {
          title: "Basic Bathing",
          icon: dog,
          extraNote: "",
          content: [
            {
              contentTitle: "Small Size Dog (<11kgs)",
              amount: "₹ 666",
            },
            {
              contentTitle: "Small Size Dog (<11kgs)",
              amount: "₹ 666",
            },
            {
              contentTitle: "Small Size Dog (<11kgs)",
              amount: "₹ 666",
            },
          ],
        },
        {
          title: "Tick & Flea Bath",
          icon: dog,
          extraNote: "",
          content: [
            {
              contentTitle: "Small Size Dog (<11kgs)",
              amount: "₹ 666",
            },
            {
              contentTitle: "Small Size Dog (<11kgs)",
              amount: "₹ 666",
            },
            {
              contentTitle: "Small Size Dog (<11kgs)",
              amount: "₹ 666",
            },
          ],
        },
      ],
    },
  ],
};

const serviceContent2 = {
  title_1: "Dog",
  title_2: "Grooming & Spa",
  image: "http://dognation.pet/wp-content/uploads/2019/07/hero_image_05-2.png",
  accordion: [
    {
      id: "1",
      accordionId: "wos-a-1",
      title: "Dog Grooming",
      description:
        "Grooming services to make your buddy feel like a king/queen.",
      type: "text",
    },

    {
      id: "2",
      accordionId: "wos-a-2",
      title: "Spa",
      description: "Let us pamper your dog the way they are supposed to be.",
      type: "text",
    },

    {
      id: "3",
      accordionId: "wos-a-3",
      title: "View Rate Card",
      type: "rateCard",
      description: [
        {
          title: "Nail Clipping & Ear Cleaning",
          icon: dog,
          extraNote: "",
          content: [
            {
              contentTitle: "Medium Size Dog (11 – 25kgs)",
              amount: "₹ 66666",
            },
            {
              contentTitle: "Small Size Dog (<11kgs)",
              amount: "₹ 666",
            },
            {
              contentTitle: "Small Size Dog (<11kgs)",
              amount: "₹ 666",
            },
          ],
        },
        {
          title: "Tick & Flea Bath",
          icon: dog,
          extraNote:
            "*Minimum number of sessions required are 8. Any extra session can be decided post evaluation at Rs. 999 per extra session",
          content: [
            {
              contentTitle: "Basic obedience training (8 sessions)",
              amount: "₹ 10666",
            },
          ],
        },
      ],
    },
  ],
};

const serviceContent3 = {
  title_1: "Activities &",
  title_2: "Events",
  image: "http://dognation.pet/wp-content/uploads/2019/07/hero_image_03-2.png",
  accordion: [
    {
      id: "1",
      accordionId: "wos-a-1",
      title: "Dog Grooming",
      description:
        "Grooming services to make your buddy feel like a king/queen.",
      type: "text",
    },

    {
      id: "2",
      accordionId: "wos-a-2",
      title: "Spa",
      description: "Let us pamper your dog the way they are supposed to be.",
      type: "text",
    },

    {
      id: "3",
      accordionId: "wos-a-3",
      title: "View Rate Card",
      description:
        "A customized approach to all your Dog’s needs and a strict cleanliness regiment ensure that your baby can make new friends without a care in the world!",
      type: "text",
    },
  ],
};

const serviceOptionData1 = {
  id: "dog-day-care",
  icon: dog,
  headerText: "DOG DAYCARE & BOARDING",
  title: "Our Paw Pals are experienced professionals expert at",
  content: [
    "Reading body language",
    "Managing group play and off leash control",
    "Encouraging appropriate play",
    "Safely stopping rough play",
    "Handling both dominant & submissive dogs",
  ],
};

const serviceOptionData2 = {
  id: "dog-grooming-spa",
  icon: dog,
  headerText: "DOG GROOMING & SPA",
  title: "Our salon & spa will help your pooch",
  content: [
    "Reading body language",
    "Managing group play and off leash control",
    "Encouraging appropriate play",
    "Safely stopping rough play",
    "Handling both dominant & submissive dogs",
  ],
};

const serviceOptionData3 = {
  id: "dog-swimming-activities",
  icon: dog,
  headerText: "SWIMMING & ACTIVITIES",
  title: "We offer engaging and fun activities for your buddy",
  content: [
    "Reading body language",
    "Managing group play and off leash control",
    "Encouraging appropriate play",
    "Safely stopping rough play",
    "Handling both dominant & submissive dogs",
  ],
};

const serviceOptionData4 = {
  id: "dog-health-hygiene",
  icon: dog,
  headerText: "HEALTHY & HYGIENIC MEALS",
  title: "PawPlate! Introducing fresh human-grade food for your furry friend",
  content: [
    "Reading body language",
    "Managing group play and off leash control",
    "Encouraging appropriate play",
    "Safely stopping rough play",
    "Handling both dominant & submissive dogs",
  ],
};

export { serviceContent1, serviceContent2, serviceContent3 };
export {
  serviceOptionData1,
  serviceOptionData2,
  serviceOptionData3,
  serviceOptionData4,
};
