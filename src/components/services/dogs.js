import React, { forwardRef, useContext, useEffect, useRef } from "react";
import { Link } from "react-router-dom";
import { SafeAreaContext } from "../utiles/saveArea";
import FoodTile from "./comps/foodTile";
import ServiceContent from "./comps/serviceContent";
import ServiceOption from "./comps/serviceOption";
import {
  serviceContent1,
  serviceContent2,
  serviceContent3,
} from "./serviceData";
import "./styles.scss";

const DogsService = forwardRef((props, ref) => {
  const { globalState, setGlobalState } = useContext(SafeAreaContext);
  const dogCareRef = useRef(null); //dog-day-care
  const dogSpaRef = useRef(null); // dog-grooming-spa
  const dogActivitiesRef = useRef(null); //dog-swimming-activities
  const dogHygieneRef = useRef(null); //dog-health-hygiene

  useEffect(() => {
    autoScroll();
    setGlobalState("home"); //default
  }, [globalState]);

  const autoScroll = () => {
    switch (globalState) {
      case "dog-day-care": {
        if (!dogCareRef) return;
        dogCareRef.current.scrollIntoView({ behavior: "smooth" });
        break;
      }
      case "dog-grooming-spa": {
        if (!dogSpaRef) return;
        dogSpaRef.current.scrollIntoView({ behavior: "smooth" });
        break;
      }
      case "dog-swimming-activities": {
        if (!dogActivitiesRef) return;
        dogActivitiesRef.current.scrollIntoView({ behavior: "smooth" });
        break;
      }
      case "dog-health-hygiene": {
        if (!dogHygieneRef) return;
        dogHygieneRef.current.scrollIntoView({ behavior: "smooth" });
        break;
      }
      default: {
        break;
      }
    }
  };

  return (
    <div className="service-container">
      <div className="service-header-container-background" />
      <div className="service-header-container">
        <div className="service-header-margin">
          <Link to="/">
            <p className="service-header-text1">HOME</p>
          </Link>
          <p className="service-header-text2">For Dogs</p>
        </div>
      </div>
      <div className="service-image-container">
        <div className="service-image-container-wrapper">
          <div className="service-image-text-container">
            <p className="service-image-text service-image-text-1">
              SERVICES FOR DOGS
            </p>
            <p className="service-image-text service-image-text-2">
              The Highest Possible
            </p>
            <p className="service-image-text service-image-text-2">
              Standards Of Pet Care
            </p>
            {/* <p className="service-image-text service-image-text-3">
              Book a free session with us and participate in our dog-orientation
              program.
            </p> */}
          </div>
        </div>
      </div>
      <div className="service-content-container-wrapper">
        <div className="service-content-container">
          <div className="service-body-wrapper">
            <div className="service-body">
              <ServiceOption />
              <ServiceContent data={serviceContent1} ref={dogCareRef} />
              <ServiceContent data={serviceContent2} ref={dogSpaRef} />
              <ServiceContent data={serviceContent3} ref={dogActivitiesRef} />
              <FoodTile ref={dogHygieneRef} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
});

export default DogsService;
