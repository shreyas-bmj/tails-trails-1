import Checkbox from "@material-ui/core/Checkbox";
import MenuItem from "@material-ui/core/MenuItem";
import TextField from "@material-ui/core/TextField";
import { useFormik } from "formik";
import React, { useState } from "react";
import { Link } from "react-router-dom";
import { gender, service, timings, validations } from "./formData";
import axios from 'axios';
import CircularProgress from "@material-ui/core/CircularProgress";
import "./styles.scss";

export default function UserForm() {
  const myFormik = useFormik({
    initialValues: {
      firstName: "",
      lastName: "",
      email: "",
      phone: "",
      dogPatronsName: "",
      dogPatronsBreed: "",
      gender: "",
      dogPatronsAge: "",
      appointmentDate: "",
      appointmentTime: "",
      service: "",
      additionalInfo: "",
    },
    validate: validations,
    onSubmit: (values) => {
      if (!checked) {
        setError(true);
      } else {
        setError(false);
        //submit
        onFormSubmit(values);
      }
    },
  });

  const [checked, setChecked] = useState(false);
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(false);

  const handleChange = (event) => {
    setChecked(event.target.checked);
  };

  const onFormSubmit = async (values) => {
    setLoading(true);
    try {
      const res = await axios.post(
        `${process.env.React_App_BASE_URL}/bookings/create`,values,
       
      );
      setLoading(false);
      if (res.status === 201) {
        console.log("my erros ***** ", res);
        alert("Query has been Submitted successfully");
      } else {
        alert("Something went wrong. Please try again.");
      }
    } catch (e) {
      setLoading(false);
      console.log("my erros ^^^^^^^^^^ ", e.response);
      alert("Something went wrong. Please try again.");
    }
  };
  return (
    <div className="user-form-container">
      <div className="user-form-wrapper">
        <div className="user-form-text-container">
          <p className="user-form-text-1">Book a visit already…</p>
          <p className="user-form-text-2">for dog’s sake!</p>
        </div>
        <div className="user-form-body">
          <form className="user-form-form" onSubmit={myFormik.handleSubmit}>
            <div className="user-form-textField-container">
              <TextField
                className="user-form-textField user-form-textField-extra"
                required
                id="firstName"
                name="firstName"
                label="First Name"
                size="medium"
                onChange={myFormik.handleChange}
                value={myFormik.values.firstName}
                helperText={
                  myFormik.touched.firstName && myFormik.errors.firstName
                    ? myFormik.errors.firstName
                    : ""
                }
                variant="outlined"
              />
              <TextField
                className="user-form-textField"
                id="lastName"
                name="lastName"
                label="Last Name"
                size="medium"
                onChange={myFormik.handleChange}
                value={myFormik.values.lastName}
                variant="outlined"
              />
            </div>
            <div className="user-form-textField-container">
              <TextField
                className="user-form-textField user-form-textField-extra"
                required
                id="email"
                name="email"
                label="Email"
                size="medium"
                onChange={myFormik.handleChange}
                value={myFormik.values.email}
                helperText={
                  myFormik.touched.email && myFormik.errors.email
                    ? myFormik.errors.email
                    : ""
                }
                variant="outlined"
              />
              <TextField
                className="user-form-textField"
                required
                id="phone"
                name="phone"
                label="Phone"
                size="medium"
                type="number"
                onChange={myFormik.handleChange}
                value={myFormik.values.phone}
                helperText={
                  myFormik.touched.phone && myFormik.errors.phone
                    ? myFormik.errors.phone
                    : ""
                }
                variant="outlined"
              />
            </div>
            <div className="user-form-textField-container">
              <TextField
                className="user-form-textField user-form-textField-extra"
                required
                id="dogPatronsName"
                name="dogPatronsName"
                label="Dog Patron's Name"
                size="medium"
                onChange={myFormik.handleChange}
                value={myFormik.values.dogPatronsName}
                helperText={
                  myFormik.touched.dogPatronsName &&
                  myFormik.errors.dogPatronsName
                    ? myFormik.errors.dogPatronsName
                    : ""
                }
                variant="outlined"
              />
              <TextField
                className="user-form-textField"
                required
                id="dogPatronsBreed"
                name="dogPatronsBreed"
                label="Dog Patron's Breed"
                size="medium"
                onChange={myFormik.handleChange}
                value={myFormik.values.dogPatronsBreed}
                helperText={
                  myFormik.touched.dogPatronsBreed &&
                  myFormik.errors.dogPatronsBreed
                    ? myFormik.errors.dogPatronsBreed
                    : ""
                }
                variant="outlined"
              />
            </div>
            <div className="user-form-textField-container">
              <TextField
                className="user-form-textField user-form-textField-extra"
                required
                id="gender"
                name="gender"
                label="Gender"
                size="medium"
                onChange={myFormik.handleChange}
                value={myFormik.values.gender}
                helperText={
                  myFormik.touched.gender && myFormik.errors.gender
                    ? myFormik.errors.gender
                    : ""
                }
                variant="outlined"
                select
              >
                {gender.map((option) => (
                  <MenuItem key={option.value} value={option.value}>
                    <p className="user-form-textField-yellow">{option.label}</p>
                  </MenuItem>
                ))}
              </TextField>
              <TextField
                className="user-form-textField"
                required
                id="dogPatronsAge"
                name="dogPatronsAge"
                label="Dog Patron's Age"
                size="medium"
                type="number"
                onChange={myFormik.handleChange}
                value={myFormik.values.dogPatronsAge}
                helperText={
                  myFormik.touched.dogPatronsAge &&
                  myFormik.errors.dogPatronsAge
                    ? myFormik.errors.dogPatronsAge
                    : ""
                }
                variant="outlined"
              />
            </div>
            <div className="user-form-textField-container">
              <TextField
                className="user-form-textField user-form-textField-extra"
                required
                id="appointmentDate"
                name="appointmentDate"
                label="Date of Appointment"
                size="medium"
                type="date"
                onChange={myFormik.handleChange}
                value={myFormik.values.appointmentDate}
                helperText={
                  myFormik.touched.appointmentDate &&
                  myFormik.errors.appointmentDate
                    ? myFormik.errors.appointmentDate
                    : ""
                }
                variant="outlined"
                InputLabelProps={{ shrink: true }}
              />
              <TextField
                className="user-form-textField"
                required
                id="appointmentTime"
                name="appointmentTime"
                label="Time"
                size="medium"
                onChange={myFormik.handleChange}
                value={myFormik.values.appointmentTime}
                helperText={
                  myFormik.touched.appointmentTime &&
                  myFormik.errors.appointmentTime
                    ? myFormik.errors.appointmentTime
                    : ""
                }
                variant="outlined"
                select
              >
                {timings.map((option) => (
                  <MenuItem key={option.value} value={option.value}>
                    <p className="user-form-textField-yellow">{option.label}</p>
                  </MenuItem>
                ))}
              </TextField>
            </div>
            {/* <div className="user-form-textField-container">
              <p className="user-form-textField-1 user-form-textField">
                Dog patron’s medical records
              </p>
              <TextField
                className="user-form-textField"
                id="user-form-file"
                // name="phone"
                // label="Phone"
                size="medium"
                type="file"
                // onChange={myFormik.handleChange}
                // value={myFormik.values.name}
                // helperText={
                //   myFormik.touched.name && myFormik.errors.name
                //     ? myFormik.errors.name
                //     : ""
                // }
                variant="outlined"
              />
            </div> */}
            <div className="user-form-textField-container">
              <TextField
                className="user-form-textField"
                required
                id="service"
                name="service"
                label="Choose a service"
                size="medium"
                onChange={myFormik.handleChange}
                value={myFormik.values.service}
                helperText={
                  myFormik.touched.service && myFormik.errors.service
                    ? myFormik.errors.service
                    : ""
                }
                variant="outlined"
                select
              >
                {service.map((option) => (
                  <MenuItem key={option.value} value={option.value}>
                    <p className="user-form-textField-yellow">{option.label}</p>
                  </MenuItem>
                ))}
              </TextField>
            </div>
            <div className="user-form-textField-container">
              <TextField
                className="user-form-textField"
                id="additionalInfo"
                name="additionalInfo"
                label="We’d love to know more about your pooch, maybe a funny story?"
                size="medium"
                onChange={myFormik.handleChange}
                value={myFormik.values.additionalInfo}
                multiline
                rowsMax={7}
                variant="outlined"
              />
            </div>
            <div className="user-form-tAndc-wrapper">
              <div className="user-form-tAndc-container">
                <Checkbox
                  className="user-form-tAndc-checkbox"
                  onChange={handleChange}
                  value={checked}
                />
                <p className="user-form-tAndc-text">
                  By submitting this form, you agree to our
                  <Link target="_blank" to="/terms-of-service">
                    <span className="user-form-tAndc user-form-tAndc-yellow">
                      &nbsp;Terms & Conditions
                    </span>
                  </Link>{" "}
                  .*
                </p>
              </div>
              {error ? (
                <p className="user-form-tAndc-error">Please check T&S</p>
              ) : (
                <></>
              )}
            </div>
            <div className="user-form-button-container">
              {!loading ? (
                <button className="user-form-button" type="submit">
                  Book Now
                </button>
              ) : (
                <CircularProgress />
              )}
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
