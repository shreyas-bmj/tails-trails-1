import React from "react";
import "./styles.scss";

export default function ContactUsTile({ data }) {
  return (
    <div className="cu-tile-container">
      <div className="cu-tile-image-container">
        <img className="cu-tile-image" src={data.icon} />
      </div>
      <div className="cu-tile-body">
        <p className="cu-tile-body-title">{data.title}</p>
        <div className="cu-tile-body-text-container">
          <ul ul className="cu-tile-body-text-ul">
            {data.content?.map((val, index) => (
              <li
                className="cu-tile-body-text-li"
                key={`ContactUsTile-${index.toString()}`}
              >
                {val}
              </li>
            ))}
          </ul>
        </div>
      </div>
    </div>
  );
}
