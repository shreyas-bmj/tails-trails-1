const timings = [
  {
    value: "10:00",
    label: "10:00",
  },
  {
    value: "11:00",
    label: "11:00",
  },
  {
    value: "12:00",
    label: "12:00",
  },
  {
    value: "13:00",
    label: "13:00",
  },
  {
    value: "14:00",
    label: "14:00",
  },
  {
    value: "15:00",
    label: "15:00",
  },
  {
    value: "16:00",
    label: "16:00",
  },
  {
    value: "17:00",
    label: "17:00",
  },
  {
    value: "18:00",
    label: "18:00",
  },
  {
    value: "19:00",
    label: "19:00",
  },
];

const service = [
  {
    value: "607ab93a84f58a7897f9aa1b",
    label: "Dog Daycare",
  },
  {
    value: "607ab93a84f58a7897f9aa1b",
    label: "Dog Boarding",
  },
  {
    value: "607ab93a84f58a7897f9aa1b",
    label: "Grooming & Spa ",
  },
  {
    value: "607ab93a84f58a7897f9aa1b",
    label: "Swimming & Activities",
  },
];

const gender = [
  { value: "male", label: "Male" },
  { value: "female", label: "Female" },
];

const validations = (values) => {
  let formError = {};
  const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  if (!values.firstName) {
    formError.firstName = "First Name required";
  }
  if (!values.email) {
    formError.email = "Email required";
  } else if (!re.test(values.email)) {
    formError.email = "Email format error";
  }
  if (!values.phone) {
    formError.phone = "Phone Number required";
  }

  if (!values.dogPatronsName) {
    formError.dogPatronsName = "Dog Name required";
  }
  if (!values.dogPatronsBreed) {
    formError.dogPatronsBreed = "Dog Breed required";
  }
  if (!values.gender) {
    formError.gender = "Gender required";
  }
  if (!values.dogPatronsAge) {
    formError.dogPatronsAge = "Dog age required";
  }
  if (!values.appointmentDate) {
    formError.appointmentDate = "Date of appointment required";
  }
  if (!values.appointmentTime) {
    formError.appointmentTime = "Time of appointment required";
  }
  if (!values.service) {
    formError.service = "Type of service required";
  }
  return formError;
};

export { service, gender, timings, validations };
