import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import Dog from "../../assets/icons/dog.svg";
import ContactUsTile from "./comps/contactUsTile";
import UserForm from "./comps/userForm";
import "./styles.scss";

const data1 = {
  icon: Dog,
  title: "OPENING HOURS",
  content: [
    "Dogs: 9:30 am – 9:30 pm (all days).",
    "Hoo-mans: 9:30 am – 9:30 pm (all days).",
  ],
};

const data2 = {
  icon: Dog,
  title: "Boarding",
  content: [
    "Drop – 24x7.",
    "Pick-up – Till 10:00 pm.",
    "In the event of extraneous circumstances please inform 1 hour before slotted pick-up time.",
    "Kindly note, you will be charged for half day if pick-up is after 11:30 pm.",
  ],
};

export default function ContactUs() {
  useEffect(() => {
    window.scrollTo({ top: 0, behavior: "smooth" });
  }, []);

  return (
    <div className="cu-container">
      <div className="cu-header-container-background" />
      <div className="cu-header-container">
        <div className="cu-header-margin">
          <Link to="/">
            <p className="cu-header-text1">HOME</p>
          </Link>
          <p className="cu-header-text2">Contact Us</p>
        </div>
      </div>
      <div className="cu-image-container">
        <div className="cu-image-container-wrapper">
          <div className="cu-image-text-container">
            <p className="cu-image-text cu-image-text-1">CONTACT US</p>
            <p className="cu-image-text cu-image-text-2">Come Visit Us With</p>
            <p className="cu-image-text cu-image-text-2">Your Buddy.</p>
            <p className="cu-image-text cu-image-text-3">
              Book a free session with us and participate in our dog-orientation
              program.
            </p>
          </div>
        </div>
      </div>
      <div className="cu-content-container-wrapper">
        <div className="cu-content-container">
          <div className="cu-body-wrapper">
            <div className="cu-body">
              <div className="cs-left-container">
                <ContactUsTile data={data1} />
                <p className="cs-left-container-text">
                  PICKUP AND COLLECTION TIMINGS
                </p>
                <ContactUsTile data={data2} />
                <ContactUsTile data={data2} />
              </div>
              <div className="cs-right-container">
                <UserForm />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
