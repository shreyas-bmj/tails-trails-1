import AccessAlarmsIcon from "@material-ui/icons/AccessAlarms";
import FacebookIcon from "@material-ui/icons/Facebook";
import InstagramIcon from "@material-ui/icons/Instagram";
import LocationOnIcon from "@material-ui/icons/LocationOn";
import MailOutlineIcon from "@material-ui/icons/MailOutline";
import PhoneEnabledIcon from "@material-ui/icons/PhoneEnabled";
import React, { useContext } from "react";
import { Link, useHistory } from "react-router-dom";
import LogoSVGComponent from "../../../assets/icons/logoCode";
import { APP_STRINGS } from "../../../assets/strings";
import Bone from "../bone";
import { SafeAreaContext } from "../saveArea";
import "./styles.scss";

export default function Footer() {
  const history = useHistory();

  const { globalState, setGlobalState } = useContext(SafeAreaContext);

  const onLinkClicked = (link, key) => {
    // if (key === "contactUs") {
    //   history.push("/");
    // } else {

    // }
    setGlobalState(key);
    history.push(link);
  };

  return (
    <div className="footer-container">
      <div className="footer-content-1">
        <div className="footer-logo">
          <LogoSVGComponent />
        </div>
        <div className="footer-social-media-logo-container">
          <Link to={"/"}>
            <div className="footer-social-media-logo">
              <InstagramIcon className="footer-content-icon" />
            </div>
          </Link>
          <Link to={"/"}>
            <div className="footer-social-media-logo">
              <FacebookIcon className="footer-content-icon" />
            </div>
          </Link>
          <Link to={"/"}>
            <div className="footer-social-media-logo">
              <MailOutlineIcon className="footer-content-icon" />
            </div>
          </Link>
        </div>
      </div>
      <div className="footer-content-2">
        <p className="footer-title"> {APP_STRINGS.ABOUT}</p>
        <div className="footer-content-body">
          <ul className="footer-content-ul">
            <li className="footer-content-li">
              <LocationOnIcon className="footer-content-2-icon" />
              <p className="footer-content-2-text">
                3rdCross Road, RustamBagh Layout Main Road S R Layout,
                Bengaluru, Karnataka 560017
              </p>
            </li>
            <li className="footer-content-li">
              <PhoneEnabledIcon className="footer-content-2-icon" />
              <p className="footer-content-2-text">
                +91 8879800348, 9019874462
              </p>
            </li>
            <li className="footer-content-li">
              <AccessAlarmsIcon className="footer-content-2-icon" />
              <p className="footer-content-2-text">
                9:30 am – 9:30 pm (All Days)
              </p>
            </li>
          </ul>
        </div>
      </div>
      <div className="footer-content-2">
        <p className="footer-title"> {APP_STRINGS.QUICK_LINKS}</p>
        <div className="footer-content-body">
          <ul className="footer-content-ul">
            <li
              className="footer-content-li footer-curser"
              onClick={() => onLinkClicked("/", "aboutUs")}
            >
              <p className="footer-content-2-text">About Us</p>
            </li>
            <li
              className="footer-content-li footer-curser"
              onClick={() => onLinkClicked("/", "whyOurShop")}
            >
              <p className="footer-content-2-text">Why DogNation</p>
            </li>
            <li
              className="footer-content-li footer-curser"
              onClick={() => onLinkClicked("/", "packages")}
            >
              <p className="footer-content-2-text">Packages</p>
            </li>
            <li
              className="footer-content-li footer-curser"
              onClick={() =>
                onLinkClicked("/terms-of-service", "termsAndCondition")
              }
            >
              <p className="footer-content-2-text">Terms of Service</p>
            </li>
            <li
              className="footer-content-li footer-curser"
              onClick={() => onLinkClicked("/contact-us", "contactUs")}
            >
              <p className="footer-content-2-text">Contact Us</p>
            </li>
          </ul>
        </div>
      </div>
      <div className="footer-content-3">
        <Bone type2 />
      </div>
    </div>
  );
}
