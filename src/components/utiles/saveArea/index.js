import React, { createContext, useState } from "react";
import "./styles.scss";

export const SafeAreaContext = createContext({
  globalState: {},
  setGlobalState: () => {},
});

export default function SafeArea({ children }) {
  const [globalState, setGlobalState] = useState(null);

  return (
    <SafeAreaContext.Provider
      value={{
        globalState,
        setGlobalState: (val) => {
          setGlobalState(val);
        },
      }}
    >
      {/* <div className="safeArea-container">
        <div className="safeArea-body">{children}</div>
      </div> */}
      <div className="safeArea-alternate">{children}</div>
    </SafeAreaContext.Provider>
  );
}
