import React from "react";
import { Link } from "react-router-dom";
import "./styles.scss";

export default function Bone({ type2 }) {
  return (
    <div className="bone-container">
      <Link to={"/contact-us"}>
        {type2 ? (
          <img
            className="bone-image"
            src="http://dognation.pet/wp-content/uploads/2020/03/bone-131.png"
          />
        ) : (
          <img
            className="bone-image"
            src="http://dognation.pet/wp-content/uploads/2020/03/dog-bone-yellow-111.png"
          />
        )}
      </Link>
    </div>
  );
}
