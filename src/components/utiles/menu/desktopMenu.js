import PhoneIcon from "@material-ui/icons/Phone";
import React, { useContext } from "react";
import { Link } from "react-router-dom";
import Logo from "../../../assets/icons/logo.svg";
import { APP_STRINGS } from "../../../assets/strings";
import "./styles.scss";
import { MenuContext } from "./utiles";
import LogoSVGComponent from "../../../assets/icons/logoCode";

export default function DesktopMenu({ onLinkClicked, floatingOptions }) {
  const { state, setState, hoverOpen, setHoverOpen } = useContext(MenuContext);

  return (
    <div className="menu-container flex-row ">
      <Link to={"/"} >
        {/* <img className="menu-logo" src={Logo} /> */}
        <div className="menu-logo" >
          <LogoSVGComponent />
        </div>
      </Link>
      <nav className="nav-container flex-row ">
        <ul className="menu-options-container flex-row ">
          {/* home */}
          <li
            className={`menu-option menu-li-cursor ${
              state === APP_STRINGS.HOME ? "current-item " : ""
            }
  
            `}
            onClick={() => onLinkClicked("/", "home")}
          >
            <p className="menu-text">{APP_STRINGS.HOME}</p>
            <div className="menu-option-highlight" />
          </li>
          {/* home */}
          {/* About us */}
          <li
            className={`menu-option menu-li-cursor ${
              state === APP_STRINGS.ABOUT_US ? "current-item " : ""
            }
  
            `}
            onClick={() => onLinkClicked("/", "aboutUs")}
          >
            <p className="menu-text">{APP_STRINGS.ABOUT_US}</p>
            <div className="menu-option-highlight" />
          </li>
          {/* About us */}
          {/* service */}
          <div>
            <li
              className={`menu-option menu-li-cursor ${
                state === APP_STRINGS.OUR_SERVICE ? "current-item " : ""
              }
  
              `}
              // onClick={() => onLinkClicked('/', item.linkId)}
              // aria-owns={anchorEl ? "simple-menu" : undefined}
              // aria-haspopup="true"
              // onClick={handleClick}
              // onMouseOver={handleClick}
              onMouseEnter={() => setHoverOpen(true)}
              onMouseLeave={() => setHoverOpen(false)}
            >
              <p className="menu-text">{APP_STRINGS.OUR_SERVICE}</p>
              <div className="menu-option-highlight" />
            </li>
            <div
              className={`menu-floating-container ${
                hoverOpen ? "enable-floating-menu" : ""
              }`}
            >
              {floatingOptions.map((val, index) => (
                <div
                  onMouseEnter={() => setHoverOpen(true)}
                  onMouseLeave={() => setHoverOpen(false)}
                  key={`DesktopMenu-${index.toString()}`}
                >
                  <div className="menu-floating-option">
                    <p
                      key={val.id}
                      className="menu-floating-text"
                      onClick={() => onLinkClicked(val.route, "ourService")}
                    >
                      {val.name}
                    </p>
                  </div>
                  <div
                    className={`menu-floating-separator ${
                      index === floatingOptions.length - 1
                        ? "menu-floating-separator-hide"
                        : ""
                    }`}
                  />
                </div>
              ))}
            </div>
          </div>
          {/* service */}
          {/* Contact us */}
          <li
            className={`menu-option menu-li-cursor ${
              state === APP_STRINGS.CONTACT_US ? "current-item " : ""
            }
  
            `}
            onClick={() => onLinkClicked("/contact-us", "home")}
          >
            <p className="menu-text">{APP_STRINGS.CONTACT_US}</p>
            <div className="menu-option-highlight" />
          </li>
          {/* Contact us */}
        </ul>
        <div className="phone-container-main">
          <div className="phone-container">
            <div className="phone-icon-container">
              <PhoneIcon className="phone-icon" />
            </div>
            <div className="phone-number">
              <p className="phone-number-text"> {APP_STRINGS.PHONE_NO}</p>
            </div>
          </div>
        </div>
      </nav>
    </div>
  );
}
