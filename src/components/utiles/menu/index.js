import React, { useContext, useEffect, useState } from "react";
import { useHistory, useLocation } from "react-router-dom";
import { APP_STRINGS } from "../../../assets/strings";
import { SafeAreaContext } from "../saveArea";
import DesktopMenu from "./desktopMenu";
import MobileMenu from "./mobileMenu";
import "./styles.scss";
import { MenuContext } from "./utiles";





const floatingOptions = [
  // { id: "human_123", name: "For Humans", route: "/service/humans" },
  { id: "dog_123", name: "For Dogs", route: "/service/dogs" },
];

export default function Menu() {
  const route = useLocation();
  const [state, setState] = useState(APP_STRINGS.HOME);
  const [hoverOpen, setHoverOpen] = React.useState(false);

  const history = useHistory();

  const { globalState, setGlobalState } = useContext(SafeAreaContext);

  const onLinkClicked = (link, key) => {
    setGlobalState(key);
    history.push(link);
  };

  const parsePath = () => {
    switch (route.pathname) {
      case "/":
        {
          if (globalState === "home") {
            setState(APP_STRINGS.HOME);
          } else if (globalState === "aboutUs") {
            setState(APP_STRINGS.HOME);
          } else {
            setState(APP_STRINGS.HOME);
          }
        }
        break;

      case "/about-us":
        setState(APP_STRINGS.ABOUT_US);
        break;

      case "/terms-of-service":
        setState(APP_STRINGS.TERMS_SERVICES);
        break;

      case "/contact-us":
        setState(APP_STRINGS.CONTACT_US);
        break;

      case "/service/dogs":

      case "/service/humans":
        setState(APP_STRINGS.OUR_SERVICE);
        setHoverOpen(false);
        break;

      default:
        setState(APP_STRINGS.HOME);
        break;
    }
  };

  useEffect(() => {
    parsePath();
  }, [route.pathname]);

  return (
    <MenuContext.Provider
      value={{
        state,
        setState: (val) => {
          setState(val);
        },
        hoverOpen,
        setHoverOpen: (val) => {
          setHoverOpen(val);
        },
      }}
    >
      <>
        <DesktopMenu
          onLinkClicked={onLinkClicked}
          floatingOptions={floatingOptions}
        />
        <MobileMenu
          onLinkClicked={onLinkClicked}
          floatingOptions={floatingOptions}
        />
      </>
    </MenuContext.Provider>
  );
}
