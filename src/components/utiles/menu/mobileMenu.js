import PhoneIcon from "@material-ui/icons/Phone";
import React, { useContext, useState } from "react";
import { slide as Menu } from "react-burger-menu";
import Logo from "../../../assets/icons/logo.svg";
import LogoSVGComponent from "../../../assets/icons/logoCode";
import { APP_STRINGS } from "../../../assets/strings";
import "./styles.scss";
import { MenuContext } from "./utiles";

export default function MobileMenu({ onLinkClicked, floatingOptions }) {
  const [isOpen, setOpen] = useState(false);
  const { state, setState, hoverOpen, setHoverOpen } = useContext(MenuContext);

  const onMenuItemClicked = (route, key) => {
    onLinkClicked(route, key);
    closeMenu();
  };

  const handleStateChange = (event) => {
    setOpen(event.isOpen);
  };

  const closeMenu = () => {
    setOpen(false);
  };

  const toggleMenu = () => {
    setOpen((prevState) => ({ prevState }));
  };

  return (
    <div className="mobile-menu-container">
      <Menu
        burgerButtonClassName={"mobile-menu-btn"}
        isOpen={isOpen}
        onStateChange={(event) => handleStateChange(event)}
      >
        <div className="mobile-menu-image-wrapper-menu">
          <LogoSVGComponent />
        </div>
        <div
          className="mobile-menu-text-container"
          onClick={() => onMenuItemClicked("/", "home")}
        >
          <p
            className={`mobile-menu-text ${
              state === APP_STRINGS.HOME ? "current-item " : ""
            }`}
          >
            {APP_STRINGS.HOME}
          </p>
        </div>
        <div
          className="mobile-menu-text-container"
          onClick={() => onMenuItemClicked("/", "aboutUs")}
        >
          <p
            className={`mobile-menu-text ${
              state === APP_STRINGS.ABOUT_US ? "current-item " : ""
            }`}
          >
            {APP_STRINGS.ABOUT_US}
          </p>
        </div>
        <div
          className="mobile-menu-text-container"
          onClick={() => onMenuItemClicked("/service/dogs", "ourService")}
        >
          <p
            className={`mobile-menu-text ${
              state === APP_STRINGS.OUR_SERVICE ? "current-item " : ""
            }`}
          >
            {APP_STRINGS.OUR_SERVICE}
          </p>
        </div>
        <div
          className="mobile-menu-text-container"
          onClick={() => onMenuItemClicked("/contact-us", "home")}
        >
          <p
            className={`mobile-menu-text ${
              state === APP_STRINGS.CONTACT_US ? "current-item " : ""
            }`}
          >
            {APP_STRINGS.CONTACT_US}
          </p>
        </div>

        <div className="phone-container-main">
          <div className="phone-container">
            <div className="phone-icon-container">
              <PhoneIcon className="phone-icon" />
            </div>
            <div className="phone-number">
              <p className="phone-number-text"> {APP_STRINGS.PHONE_NO}</p>
            </div>
          </div>
        </div>
      </Menu>
      <div className="mobile-menu-image-wrapper">
        {/* <img className="mobile-menu-image" src={Logo} /> */}
        <LogoSVGComponent />
      </div>
    </div>
  );
}
