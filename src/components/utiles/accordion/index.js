import Accordion from "@material-ui/core/Accordion";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import CheckIcon from "@material-ui/icons/Check";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import React from "react";
import { Link } from "react-router-dom";
import "./styles.scss";

export default function AppAccordion({ data }) {
  const [expanded, setExpanded] = React.useState(false);

  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  return (
    <div className="app-accordion-container">
      <Accordion
        expanded={expanded === data.accordionId}
        onChange={handleChange(data.accordionId)}
      >
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id={`panel1a-header-${data.id}`}
        >
          <div className="app-accordion-tick-icon-container flex-row ">
            <CheckIcon className="app-accordion-tick-icon" />
          </div>
          <p className="app-accordion-title">{data.title}</p>
        </AccordionSummary>
        <AccordionDetails>
          <p className="app-accordion-para">
            {data.description}
            <span>
              <Link>
                <p className="app-accordion-know-more">Know more</p>
              </Link>
            </span>
          </p>
        </AccordionDetails>
      </Accordion>
    </div>
  );
}
