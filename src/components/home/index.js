import React, { useContext, useEffect, useRef } from "react";
import { SafeAreaContext } from "../utiles/saveArea";
import Intro from "./comps/intro";
import OurServices from "./comps/ourServices";
import Packages from "./comps/packages";
import WhoWeAre from "./comps/whoWeAre";
import WhyOurShop from "./comps/whyOurShop";
import "./styles.scss";

export default function Home() {
  const { globalState, setGlobalState } = useContext(SafeAreaContext);

  const introRef = useRef(null);
  const wosRef = useRef(null);
  const osRef = useRef(null);
  const wwaRef = useRef(null);
  const packageRef = useRef(null);

  useEffect(() => {
    autoScroll();
    setGlobalState("home"); //default
  }, [globalState]);

  const autoScroll = () => {
    switch (globalState) {
      // case '' : {
      //   if(!introRef) return;
      //   introRef.current.scrollIntoView({behavior: 'smooth'})
      //   break;
      // }
      case "whyOurShop": {
        if (!wosRef) return;
        wosRef.current.scrollIntoView({ behavior: "smooth" });
        break;
      }
      // case '' : {
      //   if(!osRef) return;
      //   osRef.current.scrollIntoView({behavior: 'smooth'})
      //   break;
      // }
      case "aboutUs": {
        if (!wwaRef) return;
        wwaRef.current.scrollIntoView({ behavior: "smooth" });
        break;
      }
      case "packages": {
        if (!packageRef) return;
        packageRef.current.scrollIntoView({ behavior: "smooth" });
        break;
      }
      default: {
        break;
      }
    }
  };

  return (
    <div className="safeArea-container">
      <div className="home-container safeArea-body">
        <Intro ref={introRef} />
        <WhyOurShop ref={wosRef} />
        <OurServices ref={osRef} />
        <WhoWeAre ref={wwaRef} />
        <Packages ref={packageRef} />
      </div>
    </div>
  );
}
