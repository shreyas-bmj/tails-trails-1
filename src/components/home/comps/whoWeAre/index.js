import React, { forwardRef } from "react";
import "./styles.scss";

const data = {
  para: {
    p1: "We are Dog Nation. A one-stop destination for all things dog!",
    p2:
      "A 6,000 sqft ‘Poochy’ paradise with complete dog services, a café, workspace and a pool… designed solely for the comfort of Dogs and their Hooman friends right in the heart of the city!",
    p3:
      "Dog Nation is not just another destination, it’s an immersive experience for both your furry friend and you! But at the core of it, we are simply dog lovers!",
  },
  authors: [
    {
      image: "http://dognation.pet/wp-content/uploads/2020/02/Image-2.png",
      about:
        "Mr. Baruah, our in-house canine behavior consultant, who got his first furry BFF at the age of 21, went on to devote over 30 years of his life to his passion for dogs, gaining invaluable experience in canine behavior, breeding and basic training which will help our dog patrons to develop their mind, body and soul!",
      name: "C. R. Baruah",
      profession: "Head Behavior Consultant",
    },
    {
      image: "http://dognation.pet/wp-content/uploads/2020/02/Image-2.png",
      about:
        "Zahra’s mom is a dog fanatic, having been around dogs all her life and will do anything for her ‘love of dogs’! She was a marketing professional before starting DogNation and is an MBA from NMIMS.",
      name: "Aparajita Baruah",
      profession: "Co-Founder",
    },
    {
      image: "http://dognation.pet/wp-content/uploads/2020/02/Image-2.png",
      about:
        "Zahra’s dad loves to drive and has taken her to see distant magical places. He pursued his MBA in Finance from MDI and has experience in investor relations and strategic finance. Zahra loves her daddy but wishes he would give her all his food…oops sorry, all THE food!",
      name: "Ankit Sahay",
      profession: "Co-Founder",
    },
  ],
};

const WhoWeAre = forwardRef((props, ref) => {
  return (
    <div className="wwa-container" ref={ref}>
      <div className="wwa-title-container">
        <p className="home-title-text wwa-title">
          Who Are
          <span className="home-title-text-yellow wwa-title-yellow"> We ?</span>
        </p>
      </div>
      <div className="wwa-para-container">
        <p className="wwa-para wwa-para-1">{data.para.p1}</p>
        <p className="wwa-para wwa-para-2">{data.para.p2}</p>
        <p className="wwa-para wwa-para-3">{data.para.p3}</p>
      </div>
      <div className="wwa-divider" />
      <div className="wwa-author-container">
        {data.authors.map((val, index) => (
          <div className="wwa-author-main" key={`WhoWeAre-${index.toString()}`}>
            <div className="wwa-author-image-container">
              <img className="wwa-author-image" src={val.image} />
            </div>
            <p className="wwa-author-about">{val.about}</p>
            <p className="wwa-author-name">{val.name}</p>
            <p className="wwa-author-profession">{val.profession}</p>
          </div>
        ))}
      </div>
    </div>
  );
});

export default WhoWeAre;
