import React, { forwardRef } from "react";
import AppAccordion from "../../../utiles/accordion";
import Bone from "../../../utiles/bone";
import "./styles.scss";

const data = [
  {
    id: "1",
    accordionId: "wos-a-1",
    title: "For the Love of Dogs!",
    description:
      "Our ‘Paw-Pals’ are experienced professionals who understand canine body language and behavior patterns, ensuring that your loved one never feels your absence with complete love and devotion!",
  },

  {
    id: "2",
    accordionId: "wos-a-2",
    title: "In Safe Hands",
    description:
      "With over 30 years of canine behavior, breeding and training expertise, our in-house behavior consultant will ensure your buddy’s tail is always wagging!",
  },

  {
    id: "3",
    accordionId: "wos-a-3",
    title: "Eat.Play.Love.Repeat",
    description:
      "A customized approach to all your Dog’s needs and a strict cleanliness regiment ensure that your baby can make new friends without a care in the world!",
  },

  {
    id: "4",
    accordionId: "wos-a-4",
    title: "PawPlate",
    description:
      "Some hot, comforting Human-grade fresh food is just what your furry friends need to make the most of their day! We also offer customized meal plans for when they want more!",
  },

  {
    id: "5",
    accordionId: "wos-a-5",
    title: "Engaging Activities",
    description:
      "Carefully crafted activity sessions based on world renowned canine sports to give your pooch a holistic development! Physical.Mental.Social.",
  },
];

const WhyOurShop = forwardRef((props, ref) => {
  return (
    <div className="wos-container" ref={ref}>
      <div className="wos-left-container">
        <div className="wos-title-container">
          <p className="home-title-text wos-title">
            Why Dog
            <span className="home-title-text-yellow wos-title-yellow">
              {" "}
              Nation ?
            </span>
          </p>
        </div>
        <div className="wos-divider" />
        <div className="wos-description-container">
          <p className="wos-description-text">
            A 6, 000 sqft ‘Poochy’ paradise with complete Dog services, a café,
            workspace and a pool… designed solely for the comfort of Dogs and
            their Hu-maan friends right in the heart of the city !
          </p>
        </div>
        <div className="wos-divider" />
        <div className="wos-accordion-container">
          {data.map((val, index) => (
            <AppAccordion data={val} key={`WhyOurShop-${index.toString()}`} />
          ))}
        </div>
      </div>
      <div className="wos-right-container">
        <div className="wos-right-container-main">
          <div className="wos-right-container-sub-container">
            <img
              className="wos-right-image"
              src="http://dognation.pet/wp-content/uploads/2019/08/hero_image_13-1.png"
            />
            <div className="wos-right-bone-container">
              <Bone />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
});

export default WhyOurShop;
