import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import React from "react";
import { Link } from "react-router-dom";

export default function Tile({ data, onTileClick }) {
  return (
    <div className="os-tile-container">
      <img className="os-tile-image" src={data.icon} />
      <p className="os-tile-title">{data.title}</p>
      <p className="os-tile-para">{data.para}</p>
      <Link>
        <div
          className="os-tile-know-more-container"
          onClick={() => onTileClick(data.refId)}
        >
          <p className="os-tile-know-more">Know more</p>
          <ArrowForwardIcon className="os-tile-know-more-icon" />
        </div>
      </Link>
    </div>
  );
}
