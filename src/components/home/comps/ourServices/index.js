import React, { forwardRef, useContext } from "react";
import { useHistory } from "react-router-dom";
import Dog from "../../../../assets/icons/dog.svg";
import { SafeAreaContext } from "../../../utiles/saveArea";
import "./styles.scss";
import Tile from "./tile";

const data = [
  {
    id: "1",
    icon: Dog,
    title: "Day Care and Boarding",
    para:
      "Playtime, socializing sessions and hearty meals, your furry friend will thoroughly enjoy staying with us while you are gone.",
    onclick: "",
    refId: "dog-day-care",
  },
  {
    id: "2",
    icon: Dog,
    title: "Pet Spa and Grooming",
    para:
      "Bring on the paw pampering parents! Your buddy will leave squeaky clean, looking like a billion bones!",
    onclick: "",
    refId: "dog-grooming-spa",
  },
  {
    id: "3",
    icon: Dog,
    title: "Activities & Exercises",
    para:
      "An amazing pool and a curated list of activities for your pooch designed to help them realize their full potential!",
    onclick: "",
    refId: "dog-swimming-activities",
  },
  {
    id: "4",
    icon: Dog,
    title: "Dog Friendly Work Space",
    para:
      "For all the dog lovers out there, we have just the perfect vibrant, happy and Dog ruled workspace with a café with all the right amenities.",
    onclick: "",
    refId: "dog-health-hygiene",
  },
];

const OurServices = forwardRef((props, ref) => {
  const { globalState, setGlobalState } = useContext(SafeAreaContext);
  const history = useHistory();
  const onTileClick = (value) => {
    setGlobalState(value);
    history.push("/service/dogs");
  };

  return (
    <div className="os-container" ref={ref}>
      <div className="os-title-container">
        <p className="home-title-text os-title">
          Our
          <span className="home-title-text-yellow os-title-yellow">
            {" "}
            Services
          </span>
        </p>
      </div>
      <div className="os-divider" />
      <div className="os-tile">
        {data.map((val) => (
          <Tile data={val} key={val.id} onTileClick={onTileClick} />
        ))}
      </div>
    </div>
  );
});

export default OurServices;
