import CheckIcon from "@material-ui/icons/Check";
import React, { forwardRef } from "react";
import { Link } from "react-router-dom";
import Dog from "../../../../assets/icons/dog.svg";
import "./styles.scss";

const data = [
  {
    id: "1",
    package_name: "Diva",
    image: Dog,
    list: [
      "Daycare - 5 Days (*includes daily meals and video calls)",
      "4 Swimming & Activity Sessions",
      "2 Full Grooming Sessions",
    ],
    amount: "99,999",
    bgImage:
      "http://dognation.pet/wp-content/uploads/2020/02/imgonline-com-ua-ReplaceColor-BKFI9Cn4R1mAMCg.jpg",
  },

  {
    id: "2",
    package_name: "Diva",
    image: Dog,
    list: [
      "Daycare - 5 Days (*includes daily meals and video calls)",
      "4 Swimming & Activity Sessions",
      "2 Full Grooming Sessions",
    ],
    amount: "99,999",
    bgImage: "",
  },

  {
    id: "3",
    package_name: "Diva",
    image: Dog,
    list: [
      "Daycare - 5 Days (*includes daily meals and video calls)",
      "4 Swimming & Activity Sessions",
      "2 Full Grooming Sessions",
    ],
    amount: "99,999",
    bgImage:
      "http://dognation.pet/wp-content/uploads/2020/02/imgonline-com-ua-ReplaceColor-99ZyjdRlj1nUNSD.jpg",
  },

  {
    id: "4",
    package_name: "Diva",
    image: Dog,
    list: [
      "Daycare - 5 Days (*includes daily meals and video calls)",
      "4 Swimming & Activity Sessions",
      "2 Full Grooming Sessions",
    ],
    amount: "99,999",
    bgImage: "",
  },
];

const Packages = forwardRef((props, ref) => {
  return (
    <div className="packages-container" ref={ref}>
      <div className="packages-title-container">
        <p className="home-title-text packages-title">
          Packages For
          <span className="home-title-text-yellow packages-title-yellow">
            {" "}
            Your Buddy
          </span>
        </p>
      </div>
      <div className="packages-divider" />
      <div className="packages-main-container">
        {data.map((val, index) => (
          <div
            className="packages-tile-container"
            key={`Packages-${index.toString()}`}
          >
            <div
              className="packages-tile"
              style={
                index % 2 === 0
                  ? {
                      color: "white",
                      backgroundImage: `url(${val.bgImage})`,
                    }
                  : {
                      color: "black",
                    }
              }
            >
              <p className="packages-title"> {val.package_name}</p>
              <div className="packages-image-container">
                <img className="packages-image" src={val.image} />
              </div>
              <div className="packages-list-container">
                <ul className="packages-ul">
                  {val.list.map((item, index) => (
                    <div
                      className="packages-li-container"
                      key={`Packages-2-${index.toString()}`}
                    >
                      <li className="packages-li"> {item}</li>
                      <CheckIcon className="package-li-icon" />
                    </div>
                  ))}
                </ul>
              </div>
              <div className="packages-amount-container">
                <p className="packages-amount">
                  ₹ {val.amount}
                  /-
                </p>
              </div>
            </div>
          </div>
        ))}
      </div>
      <div className="packages-points-container">
        <p className="packages-point packages-point-1">
          *Packages are valid till 30 days from purchase (prices are excluding
          taxes)
        </p>
        <p className="packages-point packages-point-2">
          Don’t like any of these Packages? We got you covered…Build your own
          package{" "}
          <span>
            <Link>
              <span className="packages-point-yellow">here!</span>
            </Link>
          </span>
        </p>
        <p className="packages-point packages-point-3">
          <b>Note: </b>Swimming Session includes select activities and is a 2.5
          hours pass limited to single usage in a day. Individual Activities
          range from 20mins to 60mins sessions depending on our Dog patron’s
          interest.{" "}
        </p>
      </div>
    </div>
  );
});

export default Packages;
