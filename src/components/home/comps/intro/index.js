import React, { forwardRef } from "react";
import { Carousel } from "react-responsive-carousel";
import Bone from "../../../utiles/bone";
import "./styles.scss";
import IntroImage from '../../../../assets/icons/intro_image.svg'

const data = [
  {
    id: "1",
    image: "http://dognation.pet/wp-content/uploads/2020/02/Image-2.png",
    title: "Spa & Grooming ",
    para: "“Get ready for the pampered paw parade! Hygiene, in style”.",
  },
  {
    id: "2",
    image: "http://dognation.pet/wp-content/uploads/2020/02/Image-2.png",
    title: "Spa & Grooming ",
    para: "“Get ready for the pampered paw parade! Hygiene, in style”.",
  },
  {
    id: "3",
    image: "http://dognation.pet/wp-content/uploads/2020/02/Image-2.png",
    title: "Spa & Grooming ",
    para: "“Get ready for the pampered paw parade! Hygiene, in style”.",
  },
];

const Intro = forwardRef((props, ref) => {
  return (
    <div className="intro-container" ref={ref}>
      <div className="intro-left-container">
        <div className="intro-title-container">
          <p className="intro-title intro-title-1">The Complete</p>
          <p className="intro-title intro-title-2">Dog Experience</p>
        </div>
        <div className="intro-carousal-container">
          <Carousel
            className="intro-carousal-container-main"
            emulateTouch
            swipeable
            // autoPlay
            // interval={1000}
            infiniteLoop
            showArrows={false}
            showStatus={false}
            showIndicators={true}
            showThumbs={false}
          >
            {data.map((val) => (
              <div className="intro-carousal-div" key={val.id}>
                <img className="intro-carousal-image" src={val.image} />
                <div className="intro-carousal-text-container">
                  <p className="intro-carousal-title">{val.title}</p>
                  <p className="intro-carousal-para">{val.para}</p>
                </div>
              </div>
            ))}
          </Carousel>
        </div>
        <div className="intro-bone-container">
          <Bone />
        </div>
      </div>
      <div className="intro-right-container">
        <div className="intro-right-container-main">
          <img
            className="intro-right-image"
            src={
              IntroImage
            }
          />
        </div>
      </div>
    </div>
  );
});

export default Intro;
