import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import ContactUs from "../components/contactUs";
//screens
import Home from "../components/home";
import ServiceForDogs from "../components/services/dogs";
import TermsServices from "../components/termsServices";
import Footer from "../components/utiles/footer";
import Menu from "../components/utiles/menu";
import SafeArea from "../components/utiles/saveArea";
import "./styles.scss";



export default function Dashboard() {
  return (
    <SafeArea>
      <Router>
        <Menu />
        <div className="menu-save-area">
          <Switch>
            <Route path="/" component={Home} exact />
            <Route path="/contact-us" component={ContactUs} />
            <Route path="/terms-of-service" component={TermsServices} />
            <Route path="/service/dogs" component={ServiceForDogs} />
            <Route path="*" component={Home} /> {/** re-route to home */}
          </Switch>
        </div>
        <Footer />
      </Router>
    </SafeArea>
  );
}

/** 
 * 
 * 
 * 
 *  <Route
              path="/service"
              render={({ match: { path } }) => (
                <>
                  <Route path={`${path}/dogs`} component={ServiceForDogs} />
                  <Route path={`${path}/humans`} component={ServiceForHumans} />
                  </>
                  )}
                />
*/
